# PruebaTecnica

## Getting started

Resolucion de la Prueba Tecnica 

en el presente existe:
1. la carpeta "pregunta 1"
-se encuentra la reasolucion de la pregunta 1 en el mismo se adjunta los SQL de las consultas y el SQL de las tablas de la base de datos.
-el nombre de la base de datos es "PruebaTecnica".
2. la carpeta "pregunta 2-3-4"
-contiene en pdf la respuesta a las preguntas 2, 3, 4 de la Prueba
3. la carpeta "5-6"
-contiene el dump de la base de datos para la prueba.
-la carpeta PRUEBA es el proyecto laravel+vue
-se debe configurar el archivo .env
-para iniciar debe correr en terminal los comando "npm run dev" y "php artisan serve"
--dentro del dump se tienen como usuario de prueba para le login email: jose@luis.com password:joseluis
notas:
el primer ejercicio de la pregunta funciona con el middleware VerificarEmail.php el mismo que verifique si el usuario 
logueado cuenta con la verificacion en el campo email_verified_at en la tabla User, si se inicia sesion con el usuario antes mencionado tendra acceso a los 
menus de Usuarios, Roles, Permisos
-en caso de registrar un nuevo usuario el campo ROl ID debe estar en blanco,ese usuario podra iniciar sesion sin los permisos completos redirecionando la
vista a un mensaje "verificar aqui"
-el segundo, tercer y cuarto ejercicio de la pregunta 5 no fueron implementados
-el CRUD para las tablas Usuarios, Roles, Permisos estan habilitados
