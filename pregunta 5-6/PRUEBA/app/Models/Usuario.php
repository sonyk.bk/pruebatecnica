<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    use HasFactory;
    protected $table = 'Usuarios';
    public $timestamps = false;
    protected $fillable = ['nombre','apellido','correo','telefono','id_rol'];
}
