<?php

namespace App\Http\Middleware;

use App\Models\Session;
use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;

class VerificarSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $session = Session::where('user-id',$request->user()->id)->first();

        $hora = New Carbon($session->last_activity);

        return $next($request);
    }
}
