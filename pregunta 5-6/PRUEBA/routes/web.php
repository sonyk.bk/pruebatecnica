<?php

use App\Http\Controllers\PermisoController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\RolController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});
Route::middleware(['auth:sanctum','verified'])->get('/dashboard',function(){
    return Inertia::render('Dashboard');
})->name('dashboard');
Route::middleware(['auth:sanctum','verified'])->get('/verificaremail',function(){
    return Inertia::render('Control/Verificar');
})->name('VerificarEmail');

Route::resource('usuarios',UsuarioController::class)->middleware(['auth:sanctum','verified','verificaremail']);
Route::resource('permisos',PermisoController::class)->middleware(['auth:sanctum','verified','verificaremail']);
Route::resource('rol',RolController::class)->middleware(['auth:sanctum','verified','verificaremail']);
