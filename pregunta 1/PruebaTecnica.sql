
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


-- Base de datos: `PruebaTecnica`

CREATE TABLE `permisos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-
-- Volcado de datos para la tabla `permisos`


INSERT INTO `permisos` (`id`, `nombre`, `id_rol`) VALUES
(1, 'Configuracion General', 1),
(2, 'Menu Coordinador', 2),
(3, 'menu control', 2);


-- Estructura de tabla para la tabla `Roles`

CREATE TABLE `Roles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `id_permiso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- Volcado de datos para la tabla `Roles`


INSERT INTO `Roles` (`id`, `nombre`, `id_permiso`) VALUES
(1, 'Administrador', 1),
(2, 'Coordinador', 2),
(7, 'auxiliar', 2);


-- Estructura de tabla para la tabla `Usuarios`


CREATE TABLE `Usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `correo` varchar(200) DEFAULT NULL,
  `telefono` varchar(200) DEFAULT NULL,
  `id_rol` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Volcado de datos para la tabla `Usuarios`


INSERT INTO `Usuarios` (`id`, `nombre`, `apellido`, `correo`, `telefono`, `id_rol`) VALUES
(1, 'usuario uno', 'user', 'usuario_uno@gmail.com', '950000001', 1),
(2, 'usuario dos', 'user', 'usuario_dos@gmail.com', '950000002', 2),
(3, 'usuario tres', 'user', 'usuario_tres@gmail.com', '950000003', 7);

-- Indices de la tabla `permisos`

ALTER TABLE `permisos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permisos_FK` (`id_rol`);


-- Indices de la tabla `Roles`

ALTER TABLE `Roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Roles_FK` (`id_permiso`);


-- Indices de la tabla `Usuarios`

ALTER TABLE `Usuarios`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_rol` (`id_rol`);

-- AUTO_INCREMENT de la tabla `permisos`

ALTER TABLE `permisos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `Roles`
--
ALTER TABLE `Roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `Usuarios`
--
ALTER TABLE `Usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;


-- Filtros para la tabla `permisos`

ALTER TABLE `permisos`
  ADD CONSTRAINT `permisos_FK` FOREIGN KEY (`id_rol`) REFERENCES `Roles` (`id`);

--
-- Filtros para la tabla `Roles`
--
ALTER TABLE `Roles`
  ADD CONSTRAINT `Roles_FK` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`id`);

--
-- Filtros para la tabla `Usuarios`
--
ALTER TABLE `Usuarios`
  ADD CONSTRAINT `Usuarios_FK` FOREIGN KEY (`id_rol`) REFERENCES `Roles` (`id`);
COMMIT;

